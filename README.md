<div style="text-align: center;">
    <img src="when-piece.png" alt="Meme about when one piece will end" style="max-height: 100%;"/>
</div>

# What?
Monke waits for One Piece to finish

# Who?
Monke (aka me)

# Where?
Hosted on [https://when.piece.jfaldanam.xyz/](https://when.piece.jfaldanam.xyz/)

# Why?
I am waiting for One Piece to end, so I can watch it in one go

# When?
That is what I would like to know too...
